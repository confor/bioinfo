#!/bin/bash

# count different gff features per $id (col $1)

set -euo pipefail

# assert(argv.length == 1)
if [[ "$#" -ne 1 ]]; then
	echo "Error: Usage: $0 file.gff" >&2
	exit 1
fi

# assert($1 is file)
if [[ ! -f "$1" ]]; then
	echo "Error: given file does not exist" >&2
	exit 1
fi

# assert($1 == gff file)
if ! [[ "$(head --bytes 14 "$1")" == '##gff-version ' ]]; then
	echo "Warn: not a gff file" >&2
	# exit 1
fi

awk '/^[^#]/ { print $3 }' "$1" | sort --numeric-sort | uniq -c
