#!/bin/bash

# check if file is a real gff file (lazy)

set -euo pipefail

# assert(argv.length == 1)
if [[ "$#" -ne 1 ]]; then
	echo "Error: Usage: $0 types.txt" >&2
	exit 1
fi

# assert($1 is file)
if [[ ! -f "$1" ]]; then
	echo "Error: given file does not exist" >&2
	exit 1
fi

awk '/^[^#]/ { print $1 "\t" $4 "\t" $5 "\t" $3 "\t" $6 "\t" $7 }' "$1"
