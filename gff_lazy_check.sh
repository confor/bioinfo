#!/bin/bash

# check if file is a real gff file (lazy)

set -euo pipefail

# assert(argv.length == 1)
if [[ "$#" -ne 1 ]]; then
	echo "Error: Usage: $0 file.gff" >&2
	exit 1
fi

# assert($1 is file)
if [[ ! -f "$1" ]]; then
	echo "Error: given file does not exist" >&2
	exit 1
fi

# check for gff header
if ! [[ "$(head --bytes 14 "$1")" == '##gff-version ' ]]; then
	echo "Error: not a gff file" >&2
	exit 1
fi

exit 0
