#!/bin/bash

# split gff rows per $type (col $3)

set -euo pipefail

# assert(argv.length == 2)
if [[ "$#" -ne 2 ]]; then
	echo "Error: missing args" >&2
	echo "Usage: $0 file.gff gff_type" >&2
	echo "Example: $0 ecoli.gff exon > ecoli_exon.txt" >&2
	exit 1
fi

# assert($1 is file)
if [[ ! -f "$1" ]]; then
	echo "Error: given file does not exist" >&2
	exit 1
fi

# assert($1 == gff file)
if ! [[ "$(head --bytes 14 "$1")" == '##gff-version ' ]]; then
	echo "Error: not a gff file" >&2
	exit 1
fi

awk '/^[^#]/ && $3 == "'"$2"'" { print $0 }' "$1"
