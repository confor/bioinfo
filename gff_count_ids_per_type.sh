#!/bin/bash

# count gff features per (id, type) tuple (col $1, $3)

set -euo pipefail

# assert(argv.length == 1)
if [[ "$#" -ne 1 ]]; then
	echo "Error: Usage: $0 file.gff" >&2
	exit 1
fi

# assert($1 is file)
if [[ ! -f "$1" ]]; then
	echo "Error: given file does not exist" >&2
	exit 1
fi

# assert($1 == gff file)
if ! [[ "$(head --bytes 14 "$1")" == '##gff-version ' ]]; then
	echo "Warn: not a gff file" >&2
#	exit 1
fi

awk '/^[^#]/ { print $1, $3 }' "$1" | sort --numeric-sort | uniq -c

# nicer output format, NOT greppable
# awk '/^[^#]/ { print $1, $3 }' "$1" | sort --numeric-sort | uniq -c | awk '
# BEGIN {
# 	previous=""
# }

# {
# 	if (previous != $2) {
# 		print $2
# 	}
# 	printf " %4d %s\n", $1, $3
# 	previous=$2
# }
# '
